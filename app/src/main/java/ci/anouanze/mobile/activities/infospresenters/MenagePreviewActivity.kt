package ci.anouanze.mobile.activities.infospresenters

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import ci.anouanze.mobile.R
import ci.anouanze.mobile.activities.forms.ParcelleActivity
import ci.anouanze.mobile.activities.forms.ProducteurMenageActivity
import ci.anouanze.mobile.models.ProducteurMenageModel
import ci.anouanze.mobile.repositories.databases.AnouanzeRoomDatabase
import ci.anouanze.mobile.tools.Commons
import com.blankj.utilcode.util.ActivityUtils
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.synthetic.main.activity_menage_preview.*
import kotlinx.android.synthetic.main.ombrage_items_list.*

class MenagePreviewActivity : AppCompatActivity() {


    var menageData = ProducteurMenageModel()
    val draftDao = AnouanzeRoomDatabase.getDatabase(this)?.draftedDatasDao()
    var draftID = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menage_preview)

        intent?.let {
            try {
                menageData = it.getParcelableExtra("preview")!!
                draftID = it.getIntExtra("draft_id", 0)

                menageData.let { menage ->
                    labelCodeProducteurMenagePreview.text = menage.codeProducteur
                    labelLocaliteMenagePreview.text = menage.localiteNom
                    labelProducteurMenagePreview.text = menage.producteurNomPrenoms
                    labelQuartierMenagePreview.text = menage.quartier
                    labelEnergieMenagePreview.text = menage.sources_energies_id

                    try {
                        if (menage.sources_energies_id.toString().uppercase().contains("BOIS")) {
                            linearEnergieBoisWeekCountContainerMenagePreview.visibility = VISIBLE
                            labelEnergieBoisWeekCountMenagePreview.text = menage.boisChauffe
                        } else {
                            linearEnergieBoisWeekCountContainerMenagePreview.visibility = GONE
                        }
                    } catch (ex: Exception) {
                        FirebaseCrashlytics.getInstance().recordException(ex)
                    }

                    labelOrdureLieuMenagePreview.text = menage.ordures_menageres_id
                    labelSeparationDechetYesNoMenagePreview.text = menage.separationMenage
                    labelGestionEauMenagePreview.text = menage.eauxToillette
                    labelGestionVaisselleMenagePreview.text = menage.eauxVaisselle
                    labelWCYesNoMenagePreview.text = menage.wc
                    labellieuEauPortableMenagePreview.text = menage.sources_eaux_id
                    labelTraiterChampsMenagePreview.text = menage.traitementChamps

                    try {
                        if (menage.traitementChamps == "oui") {
                            labelTraiteYourselfMachineMenagePreview.text = menage.type_machines_id

                            if (menage.machine.toString().uppercase().contains("ATOMISATEUR")) {
                                labelTraiteChampsMachineAtomisateurStatusMenagePreview.text = "oui"
                                linearTraiteChampsMachineAtomisateurStatusContainerMenagePreview.visibility =
                                    VISIBLE
                            } else {
                                linearTraiteChampsMachineAtomisateurStatusContainerMenagePreview.visibility =
                                    GONE
                            }

                            labelEquipementProtectionMenagePreview.text = menage.equipements

                            linearTraiteYourselfFarmYesContainerMenagePreview.visibility = VISIBLE
                            linearTraitYourselfFarmNoContainer.visibility = GONE
                        } else {
                            labelTraitYourselfFarmNoOtherNameMenagePreview.text =
                                menage.nomPersonneTraitant
                            labelTraitYourselfFarmNoOtherNumberMenagePreview.text =
                                menage.numeroPersonneTraitant

                            linearTraiteYourselfFarmEquipmentYesNoContainerMenagePreview.visibility =
                                GONE
                            linearTraiteYourselfFarmYesContainerMenagePreview.visibility = GONE
                            linearTraiteChampsMachineAtomisateurStatusContainerMenagePreview.visibility =
                                GONE
                            linearTraitYourselfFarmNoContainer.visibility = VISIBLE
                        }
                    } catch (ex: Exception) {
                        FirebaseCrashlytics.getInstance().recordException(ex)
                    }

                    labelFemmeActiviteYesNoMenagePreview.text = menage.champFemme

                    try {
                        if (menage.activiteFemme.toString().contains("oui")) {
                            labelFemmeActiviteYesNomMenagePreview.text = menage.nomActiviteFemme
                            linearFemmeActiviteYesContainerMenagePreview.visibility = VISIBLE
                        } else {
                            linearFemmeActiviteYesContainerMenagePreview.visibility = GONE
                        }
                    } catch (ex: Exception) {
                        FirebaseCrashlytics.getInstance().recordException(ex)
                    }

                    labelFemmeCacaoSuperficieMenagePreview.text = menage.superficieCacaoFemme

                    try {
                        menage.superficieCacaoFemme = if (menage.superficieCacaoFemme.toString()
                                .isEmpty()
                        ) "0.0" else menage.superficieCacaoFemme.toString().toDouble().toString()
                        menage.nombreHectareFemme = if (menage.nombreHectareFemme.toString()
                                .isEmpty()
                        ) "0.0" else menage.nombreHectareFemme.toString().toDouble().toString()

                        if ((menage.superficieCacaoFemme.toString().toDouble()) == 0.0) {
                            labelProducteurFemmeDonChampsYesNoMenagePreview.text =
                                if (menage.nombreHectareFemme.toString()
                                        .toDouble() > 0.0
                                ) "oui" else "non"
                            labelProducteurFemmeDonChampsYesHectarMenagePreview.text =
                                menage.nombreHectareFemme
                            linearFemmeCacaoHectarUnderZeroContainerMenagePreview.visibility =
                                VISIBLE
                            linearProducteurFemmeDonChampsYesHectarContainerMenagePreview.visibility =
                                VISIBLE
                        } else {
                            linearFemmeCacaoHectarUnderZeroContainerMenagePreview.visibility = GONE
                            linearProducteurFemmeDonChampsYesHectarContainerMenagePreview.visibility =
                                GONE
                        }
                    } catch (ex: Exception) {
                        FirebaseCrashlytics.getInstance().recordException(ex)
                    }

                    clickSaveMenagePreview.setOnClickListener {
                        Commons.showMessage(
                            "Etes-vous sur de vouloir faire ce enregistrement ?",
                            this,
                            showNo = true,
                            callback = {
                                try {
                                    AnouanzeRoomDatabase.getDatabase(this)?.producteurMenageDoa()
                                        ?.insert(menage)
                                    draftDao?.completeDraft(draftID)
                                    Commons.synchronisation(type = "menage", this)
                                    Commons.showMessage(
                                        "Menage enregistrée avec succes !",
                                        this,
                                        finished = true,
                                        callback = {})
                                } catch (ex: Exception) {
                                    FirebaseCrashlytics.getInstance().recordException(ex)
                                }
                            },
                            finished = false
                        )

                        ActivityUtils.finishActivity(ProducteurMenageActivity::class.java)
                    }


                    clickCloseMenagePreview.setOnClickListener {
                        finish()
                    }
                }
            } catch (ex: Exception) {
                FirebaseCrashlytics.getInstance().recordException(ex)
            }
        }
    }
}
