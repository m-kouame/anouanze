package ci.anouanze.mobile.activities.lists

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import ci.anouanze.mobile.R
import ci.anouanze.mobile.activities.forms.SuiviParcelleActivity
import ci.anouanze.mobile.adapters.ProducteurAdapter
import ci.anouanze.mobile.adapters.SuiviParcelleAdapter
import ci.anouanze.mobile.models.SuiviParcelleModel
import ci.anouanze.mobile.repositories.databases.AnouanzeRoomDatabase
import ci.anouanze.mobile.repositories.databases.daos.SuiviParcelleDao
import ci.anouanze.mobile.tools.Constants
import com.blankj.utilcode.util.ActivityUtils
import com.blankj.utilcode.util.SPUtils
import kotlinx.android.synthetic.main.activity_producteurs_list.*
import kotlinx.android.synthetic.main.activity_suivi_pacelles_list.*
import org.joda.time.DateTime

class SuiviPacellesListActivity : AppCompatActivity() {


    var suivisList: MutableList<SuiviParcelleModel>? = null
    var suiviParcelleDao: SuiviParcelleDao? = null
    var suiviAdapter: SuiviParcelleAdapter? = null


    fun retrieveDatas() {
        suivisList = mutableListOf()
        suiviParcelleDao = AnouanzeRoomDatabase.getDatabase(this)?.suiviParcelleDao()

        suivisList = suiviParcelleDao?.getUnSyncedAll(agentID = SPUtils.getInstance().getInt(
            Constants.AGENT_ID, 0).toString())

        suiviAdapter = SuiviParcelleAdapter(suivisList)

        recyclerSuiviParcelles.adapter = suiviAdapter
        recyclerSuiviParcelles.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        labelLastSynchronisationSuiviParcelles.text = resources.getString(R.string.last_synchronisation_date, DateTime.now().toString("HH:mm:ss"))

        suivisList?.let {
            if (it.isEmpty()) {
                recyclerSuiviParcelles.visibility = View.GONE
                linearEmptyContainerSuiviParcellesList.visibility = View.VISIBLE
            } else {
                recyclerSuiviParcelles.visibility = View.VISIBLE
                linearEmptyContainerSuiviParcellesList.visibility = View.GONE
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_suivi_pacelles_list)


        imgAddSuivi.setOnClickListener {
            ActivityUtils.startActivity(SuiviParcelleActivity::class.java)
        }

        imgCloseSuivi.setOnClickListener {
            finish()
        }

        retrieveDatas()
    }
}
