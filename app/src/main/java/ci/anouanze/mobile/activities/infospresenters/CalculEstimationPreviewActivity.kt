package ci.anouanze.mobile.activities.infospresenters

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ci.anouanze.mobile.R
import ci.anouanze.mobile.activities.forms.CalculEstimationActivity
import ci.anouanze.mobile.activities.forms.FormationActivity
import ci.anouanze.mobile.models.EstimationModel
import ci.anouanze.mobile.repositories.databases.AnouanzeRoomDatabase
import ci.anouanze.mobile.tools.Commons
import com.blankj.utilcode.util.ActivityUtils
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.synthetic.main.activity_calcul_estimation.*
import kotlinx.android.synthetic.main.activity_calcul_estimation_preview.*

class CalculEstimationPreviewActivity : AppCompatActivity() {


    var estimationDatas: EstimationModel? = null
    val draftDao = AnouanzeRoomDatabase.getDatabase(this)?.draftedDatasDao()
    var draftID = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calcul_estimation_preview)

        intent?.let {
            try {
                estimationDatas = it.getParcelableExtra("preview")
                draftID = it.getIntExtra("draft_id", 0)

                estimationDatas?.let { estimation ->
                    labelLocaliteNomEstimationPreview.text = estimation.localiteNom
                    labelCampagneNomEstimationPreview.text = estimation.campagnesNom
                    labelProducteurNomEstimationPreview.text = estimation.producteurNom
                    labelParclelleNomEstimationPreview.text = estimation.parcelleNom
                    labelSuperficieEstimationPreview.text = estimation.superficie
                    labelPiedA1EstimationPreview.text = estimation.ea1
                    labelPiedA2EstimationPreview.text = estimation.ea2
                    labelPiedA3EstimationPreview.text = estimation.ea3
                    labelPiedB1EstimationPreview.text = estimation.eb1
                    labelPiedB2EstimationPreview.text = estimation.eb2
                    labelPiedB3EstimationPreview.text = estimation.eb3
                    labelPiedC1EstimationPreview.text = estimation.ec1
                    labelPiedC2EstimationPreview.text = estimation.ec2
                    labelPiedC3EstimationPreview.text = estimation.ec3
                    labelDateEstimationPreview.text = estimation.dateEstimation
                }

                clickSaveEstimationPreview.setOnClickListener {
                    Commons.showMessage(
                        "Etes-vous sur de vouloir faire ce enregistrement ?",
                        this,
                        showNo = true,
                        callback = {
                            AnouanzeRoomDatabase.getDatabase(this)?.estimationDao()
                                ?.insert(estimationDatas!!)
                            draftDao?.completeDraft(draftID)
                            Commons.synchronisation(type = "estimation", this)
                            Commons.showMessage(
                                "Estimation enregistrée !",
                                this,
                                finished = true,
                                callback = {})
                        },
                        finished = false
                    )

                    ActivityUtils.finishActivity(CalculEstimationActivity::class.java)
                }
            } catch (ex: Exception) {
                FirebaseCrashlytics.getInstance().recordException(ex)
            }
        }
    }
}
