package ci.anouanze.mobile.activities.infospresenters

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import ci.anouanze.mobile.R
import ci.anouanze.mobile.activities.forms.SuiviApplicationActivity
import ci.anouanze.mobile.activities.forms.SuiviParcelleActivity
import ci.anouanze.mobile.models.SuiviApplicationModel
import ci.anouanze.mobile.repositories.databases.AnouanzeRoomDatabase
import ci.anouanze.mobile.tools.Commons
import com.blankj.utilcode.util.ActivityUtils
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.synthetic.main.activity_producteur_preview.*
import kotlinx.android.synthetic.main.activity_suivi_application.*
import kotlinx.android.synthetic.main.activity_suivi_application_preview.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.io.File

class SuiviApplicationPreviewActivity : AppCompatActivity() {


    val draftDao = AnouanzeRoomDatabase.getDatabase(this)?.draftedDatasDao()
    var draftID = 0


    suspend fun loadFileToBitmap(pPath: String?, viewTarget: AppCompatImageView) {
        try {
            if (pPath?.isEmpty()!!) return

            val imgFile = File(pPath)

            if (imgFile.exists()) {
                val options = BitmapFactory.Options()
                options.inSampleSize = 8
                val myBitmap = BitmapFactory.decodeFile(imgFile.absolutePath, options)

                MainScope().launch {
                    viewTarget.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, 80, 80, false))
                }
            }
        } catch (ex: Exception) {
            FirebaseCrashlytics.getInstance().recordException(ex)
        }
    }


    suspend fun loadFileToBitmap2(pPath: String?) {
        try {
            if (pPath?.isEmpty()!!) return

            val imgFile = File(pPath)

            if (imgFile.exists()) {
                val options = BitmapFactory.Options()
                options.inSampleSize = 8
                val myBitmap = BitmapFactory.decodeFile(imgFile.absolutePath, options)


                MainScope().launch {
                    imageVersoProdPreview.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, 80, 80, false))
                }
            }
        } catch (ex: Exception) {
            FirebaseCrashlytics.getInstance().recordException(ex)
        }


    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_suivi_application_preview)

        intent?.let {
            try {
                val suiviApplication: SuiviApplicationModel? = it.getParcelableExtra("preview")
                draftID = it.getIntExtra("draft_id", 0)


                suiviApplication?.let { suiviApp ->
                    try {
                        labelApplicateurNomSuiviApplicationPreview.text = suiviApp.applicateurNom
                        labelLocaliteNomSuiviApplicationPreview.text = suiviApp.localiteNom
                        labelProducteurNomSuiviApplicationPreview.text = suiviApp.producteurNom

                        labelCampagneNomSuiviApplicationPreview.text = suiviApp.campagneNom
                        labelCultureNomSuiviApplicationPreview.text = suiviApp.cultureNom

                        labelSuperficieSuiviApplicationPreview.text = suiviApp.superficiePulverisee
                        labelProduitNomSuiviApplicationPreview.text = suiviApp.marqueProduitPulverise

                        labelDegreDangerositeSuiviApplicationPreview.text = suiviApp.degreDangerosite
                        labelRaisonApplicationSuiviApplicationPreview.text = suiviApp.raisonApplication

                        labelDelaiReentreSuiviApplicationPreview.text = suiviApp.delaisReentree
                        labelTamponYesNoSuiviApplicationPreview.text = suiviApp.zoneTampons

                        if (suiviApp.zoneTampons == "oui") linearTamponPhotoContainerSuiviApplicationPreview.visibility = View.VISIBLE
                        else linearTamponPhotoContainerSuiviApplicationPreview.visibility = View.GONE

                        labelDoucheApplicateurYesNoSuiviApplicationPreview.text = suiviApp.presenceDouche

                        if (suiviApp.presenceDouche == "oui") linearDouchePhotoContainerSuiviApplicationPreview.visibility = View.VISIBLE
                        else linearDouchePhotoContainerSuiviApplicationPreview.visibility = View.GONE

                        labelDateApplicationSuiviApplicationPreview.text = suiviApp.dateApplication
                        labelHeureDebutApplicationSuiviApplicationPreview.text = suiviApp.heureApplication
                        labelHeureFinApplicationSuiviApplicationPreview.text = suiviApp.heureApplication

                        labelRaisonApplicationSuiviApplicationPreview

                        suiviApp.photoTamponPath?.let {
                            CoroutineScope(Dispatchers.IO).launch {
                                loadFileToBitmap(suiviApp.photoTamponPath, imageTamponPhotoSuiviApplicationPreview)
                            }
                        }

                        suiviApp.photoDouchePath?.let {
                            CoroutineScope(Dispatchers.IO).launch {
                                loadFileToBitmap(suiviApp.photoDouchePath, imageDouchePhotoSuiviApplicationPreview)
                            }
                        }
                    } catch (ex: Exception) {
                        FirebaseCrashlytics.getInstance().recordException(ex)
                    }
                }

                clickCloseSuiviAplicationPreview.setOnClickListener {
                    finish()
                }

                clickSaveApplicationSuiviPreview.setOnClickListener {
                    Commons.showMessage(
                        "Etes-vous sur de vouloir faire ce enregistrement ?",
                        this,
                        showNo = true,
                        callback = {
                            AnouanzeRoomDatabase.getDatabase(this)?.suiviApplicationDao()?.insert(suiviApplication!!)
                            draftDao?.completeDraft(draftID)
                            Commons.synchronisation(type = "suiviapplication", this)
                            Commons.showMessage(
                                "Suivi d'application effectué avec succes !",
                                this,
                                finished = true,
                                callback = {})
                        },
                        finished = false
                    )

                    ActivityUtils.finishActivity(SuiviApplicationActivity::class.java)
                }
            } catch (ex: Exception) {
                FirebaseCrashlytics.getInstance().recordException(ex)
            }
        }
    }
}
