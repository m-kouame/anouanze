package ci.anouanze.mobile.activities.lists

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import ci.anouanze.mobile.R
import ci.anouanze.mobile.activities.forms.ParcelleActivity
import ci.anouanze.mobile.activities.forms.ProducteurMenageActivity
import ci.anouanze.mobile.adapters.LocaliteAdapter
import ci.anouanze.mobile.adapters.MenageAdapter
import ci.anouanze.mobile.models.LocaliteModel
import ci.anouanze.mobile.models.ProducteurMenageModel
import ci.anouanze.mobile.repositories.databases.AnouanzeRoomDatabase
import ci.anouanze.mobile.repositories.databases.daos.LocaliteDao
import ci.anouanze.mobile.repositories.databases.daos.ProducteurMenageDao
import ci.anouanze.mobile.tools.Constants
import com.blankj.utilcode.util.ActivityUtils
import com.blankj.utilcode.util.SPUtils
import kotlinx.android.synthetic.main.activity_localites_list.*
import kotlinx.android.synthetic.main.activity_menageres_list.*
import kotlinx.android.synthetic.main.activity_parcelles_list.*
import kotlinx.android.synthetic.main.activity_producteur.*
import org.joda.time.DateTime

class MenageresListActivity : AppCompatActivity() {


    var productMenagereDao: ProducteurMenageDao? = null
    var menagesList: MutableList<ProducteurMenageModel>? = null
    var menagesAdapter: MenageAdapter? = null


    fun retrieveDatas() {
        menagesList = mutableListOf()
        productMenagereDao = AnouanzeRoomDatabase.getDatabase(this)?.producteurMenageDoa()

        menagesList = productMenagereDao?.getUnSyncedAll(agentID = SPUtils.getInstance().getInt(
            Constants.AGENT_ID, 0).toString())

        menagesAdapter = MenageAdapter(menagesList)

        recyclerMenages.adapter = menagesAdapter
        recyclerMenages.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        labelLastSynchronisationMenage.text = resources.getString(
            R.string.last_synchronisation_date,
            DateTime.now().toString("HH:mm:ss")
        )

        menagesList?.let {
            if (it.isEmpty()) {
                recyclerMenages.visibility = View.GONE
                linearEmptyContainerMenagesList.visibility = View.VISIBLE
            } else {
                recyclerMenages.visibility = View.VISIBLE
                linearEmptyContainerMenagesList.visibility = View.GONE
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menageres_list)

        clickCloseMenage.setOnClickListener {
            finish()
        }

        imgAddMenage.setOnClickListener {
            ActivityUtils.startActivity(ProducteurMenageActivity::class.java)
        }
    }


    override fun onResume() {
        super.onResume()

        retrieveDatas()
    }
}
