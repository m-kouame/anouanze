package ci.anouanze.mobile.activities.infospresenters

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ci.anouanze.mobile.R
import ci.anouanze.mobile.activities.forms.LivraisonActivity
import ci.anouanze.mobile.models.LivraisonModel
import ci.anouanze.mobile.repositories.databases.AnouanzeRoomDatabase
import ci.anouanze.mobile.tools.Commons
import com.blankj.utilcode.util.ActivityUtils
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.synthetic.main.activity_livraison_preview.*
import kotlinx.android.synthetic.main.livraison_items_list.*

class LivraisonPreviewActivity : AppCompatActivity() {


    val draftDao = AnouanzeRoomDatabase.getDatabase(this)?.draftedDatasDao()
    var draftID = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_livraison_preview)

        intent?.let {
            try {
                val livraisonDatas: LivraisonModel = it.getParcelableExtra("preview")!!
                draftID = it.getIntExtra("draft_id", 0)

                livraisonDatas.let { livraison ->
                    labelLocaliteNomLivraisonPreview.text = livraison.localiteNom
                    labelProducteurLivraisonPreview.text = livraison.producteurNom
                    labelParcelleLivraisonPreview.text = livraison.parcelleNom
                    labelCampagneLivraisonPreview.text = livraison.campagneNom
                    labelDelegueLivraisonPreview.text = livraison.delegueNom

                    labelDateLivraisonPreview.text = livraison.dateLivre
                    labelVolumeLivraisonPreview.text = livraison.quantiteLivre
                    labelNombreLivraisonPreview.text = livraison.nombreSacs

                    clickSaveLivraisonPreview.setOnClickListener {
                        Commons.showMessage(
                            "Etes-vous sur de vouloir faire ce enregistrement ?",
                            this,
                            showNo = true,
                            callback = {
                                AnouanzeRoomDatabase.getDatabase(this)?.livraisonDao()
                                    ?.insert(livraison)
                                draftDao?.completeDraft(draftID)
                                Commons.synchronisation(type = "livraison", this)
                                Commons.showMessage(
                                    "Livraison enregistrée !",
                                    this,
                                    finished = true,
                                    callback = {})
                            },
                            finished = false
                        )

                        ActivityUtils.finishActivity(LivraisonActivity::class.java)
                    }

                    clickCloseLivraisonPreview.setOnClickListener {
                        finish()
                    }

                }
            } catch (ex: Exception) {
                FirebaseCrashlytics.getInstance().recordException(ex)
            }
        }
    }
}
