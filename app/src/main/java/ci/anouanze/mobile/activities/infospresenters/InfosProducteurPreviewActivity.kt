package ci.anouanze.mobile.activities.infospresenters

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import ci.anouanze.mobile.R
import ci.anouanze.mobile.activities.forms.LocaliteActivity
import ci.anouanze.mobile.activities.forms.UniteAgricoleProducteurActivity
import ci.anouanze.mobile.models.InfosProducteurDTO
import ci.anouanze.mobile.repositories.databases.AnouanzeRoomDatabase
import ci.anouanze.mobile.tools.Commons
import ci.anouanze.mobile.tools.ListConverters
import com.blankj.utilcode.util.ActivityUtils
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.synthetic.main.activity_enquete_ssrt_preview.*
import kotlinx.android.synthetic.main.activity_infos_producteur_preview.*
import kotlinx.android.synthetic.main.activity_inspection_preview.*
import kotlinx.android.synthetic.main.activity_unite_agricole_producteur.*
import java.lang.Exception

class InfosProducteurPreviewActivity : AppCompatActivity() {


    val draftDao = AnouanzeRoomDatabase.getDatabase(this)?.draftedDatasDao()
    var draftID = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_infos_producteur_preview)

        intent?.let {
            try {
                val infosProducteur: InfosProducteurDTO? = it.getParcelableExtra("preview")
                draftID = it.getIntExtra("draft_id", 0)

                infosProducteur?.run {
                    labelProducteurInfosProducteurPreview.text = this.producteursNom
                    labelCodeInfosProducteurPreview.text = this.producteursCode

                    labelJachereYesNoInfosProducteurPreview.text = this.foretsjachere

                    if (foretsjachere == "oui") {
                        linearJachereYesSuperficieContainerInfosProducteurPreview.visibility =
                            VISIBLE
                        labelJachereYesSuperficieInfosProducteurPreview.text = this.superficie
                    } else {
                        linearJachereYesSuperficieContainerInfosProducteurPreview.visibility = GONE
                    }

                    labelOthersFarmsYesNoInfosProducteurPreview.text = this.autresCultures

                    if (autresCultures == "oui") {
                        val typeculture =
                            ListConverters.stringToMutableList(this.typecultureStringify)
                        typeculture?.let { cultures ->
                            labelOthersFarmsInfosProducteurPreview.text = null
                            cultures.map { culture ->
                                labelOthersFarmsInfosProducteurPreview.text =
                                    labelOthersFarmsInfosProducteurPreview.text.toString()
                                        .plus(culture).plus(System.getProperty("line.separator"))
                            }
                        }

                        linearOthersCultureYesContainerInfosProducteursPreview.visibility = VISIBLE
                    } else {
                        linearOthersCultureYesContainerInfosProducteursPreview.visibility = GONE
                    }

                    val maladiesenfants =
                        ListConverters.stringToMutableList(this.maladiesenfantsStringify)
                    maladiesenfants?.let { maladies ->
                        labelDeseasesInfosProducteurPreview.text = null
                        maladies.map { maladie ->
                            labelDeseasesInfosProducteurPreview.text =
                                labelDeseasesInfosProducteurPreview.text.toString().plus(maladie)
                                    .plus(System.getProperty("line.separator"))
                        }
                    }

                    labelNbreWorkersInfosProducteurPreview.text = this.travailleurs
                    labelNbreWorkersUndefinedInfosProducteurPreview.text =
                        this.travailleurstemporaires
                    labelNbreWorkersDefinedInfosProducteurPreview.text = this.travailleurspermanents

                    labelChildSchoolInfosProducteurPreview.text = this.persEcole
                    labelChildUnder18InfosProducteurPreview.text = this.age18
                    labelChildScoolExtraitInfosProducteurPreview.text = this.scolarisesExtrait

                    labelActionPeopleInjuryInfosProducteurPreview.text = this.personneBlessee
                    labelPaperFarmsInfosProducteurPreview.text = this.typeDocuments
                    labelRecuHolderInfosProducteurPreview.text = this.recuAchat

                    labelMobileMoneyYesNoInfosProducteurPreview.text = this.mobileMoney

                    if (this.mobileMoney == "oui") {
                        labelMobileMoneyOperateurInfosProducteurPreview.text = this.operateurMM
                        labelMobieMoneyNumberInfosProducteurPreview.text = this.numeroCompteMM

                        linearMobileMoneyYesOperateurContainerInfosProducteurPreview.visibility =
                            VISIBLE
                        linearMobileMoneyYesNumberContainerInfosProducteurPreview.visibility =
                            VISIBLE
                    }

                    labelBuyMethodInfoProducteurPreview.text = this.paiementMM
                    labelBanqueYesNoInfosProducteurPreview.text = this.compteBanque

                    clickCloseInfosProducteurPreview.setOnClickListener {
                        finish()
                    }

                    clickSaveInfosProducteurPreview.setOnClickListener {
                        try {
                            Commons.showMessage(
                                "Etes-vous sûr de vouloir enregistrer ce contenu ?",
                                this@InfosProducteurPreviewActivity,
                                showNo = true,
                                callback = {
                                    AnouanzeRoomDatabase.getDatabase(this@InfosProducteurPreviewActivity)
                                        ?.infosProducteurDao()?.insert(this)
                                    draftDao?.completeDraft(draftID)
                                    Commons.synchronisation(
                                        type = "infos",
                                        this@InfosProducteurPreviewActivity
                                    )
                                    Commons.showMessage(
                                        "Infos enregistrée avec succes !",
                                        this@InfosProducteurPreviewActivity,
                                        finished = true,
                                        callback = {})
                                },
                                finished = false
                            )

                            ActivityUtils.finishActivity(UniteAgricoleProducteurActivity::class.java)
                        } catch (ex: Exception) {
                            Commons.showMessage(
                                "Echec enregistreent !",
                                this@InfosProducteurPreviewActivity,
                                callback = {})
                        }
                    }
                }
            } catch (ex: Exception) {
                FirebaseCrashlytics.getInstance().recordException(ex)
            }
        }
    }
}
