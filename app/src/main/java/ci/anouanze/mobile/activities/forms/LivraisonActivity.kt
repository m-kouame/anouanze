package ci.anouanze.mobile.activities.forms

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.widget.doAfterTextChanged
import ci.anouanze.mobile.R
import ci.anouanze.mobile.activities.infospresenters.LivraisonPreviewActivity
import ci.anouanze.mobile.models.*
import ci.anouanze.mobile.repositories.apis.ApiClient
import ci.anouanze.mobile.repositories.databases.AnouanzeRoomDatabase
import ci.anouanze.mobile.repositories.databases.daos.*
import ci.anouanze.mobile.repositories.datas.CommonData
import ci.anouanze.mobile.tools.AssetFileHelper
import ci.anouanze.mobile.tools.Commons
import ci.anouanze.mobile.tools.Commons.Companion.applyFilters
import ci.anouanze.mobile.tools.Commons.Companion.provideDatasSpinnerSelection
import ci.anouanze.mobile.tools.Commons.Companion.provideStringSpinnerSelection
import ci.anouanze.mobile.tools.Commons.Companion.showMessage
import ci.anouanze.mobile.tools.Constants
import com.blankj.utilcode.util.ActivityUtils
import com.blankj.utilcode.util.GsonUtils
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.SPUtils
import kotlinx.android.synthetic.main.activity_formation.*
import kotlinx.android.synthetic.main.activity_livraison.*
import kotlinx.android.synthetic.main.activity_parcelle.*
import kotlinx.android.synthetic.main.activity_producteur.*
import kotlinx.android.synthetic.main.activity_suivi_application.*
import kotlinx.android.synthetic.main.activity_suivi_parcelle.*
import org.apache.commons.lang3.StringUtils
import java.util.*

class LivraisonActivity : AppCompatActivity() {


    companion object {
        const val TAG = "LivraisonActivity.kt"
    }


    var localiteDao: LocaliteDao? = null
    var campagneDao: CampagneDao? = null
    var producteurDao: ProducteurDao? = null
    var parcelleDao: ParcelleDao? = null
    var livraisonDao: LivraisonDao? = null
    var delegueDao: DelegueDao? = null
    var typeProduitDao: TypeProduitDao? = null
    var magasinDao: MagasinDao? = null

    var localitesList: MutableList<LocaliteModel>? = null
    var magasinsList: MutableList<MagasinModel>? = mutableListOf()
    var deleguesList: MutableList<DelegueModel>? = null
    var producteursList: MutableList<ProducteurModel>? = null
    var campagnesList: MutableList<CampagneModel>? = mutableListOf()
    var parcellesList: MutableList<ParcelleModel>? = mutableListOf()

    var localiteSelected = ""
    var localiteIdSelected = ""
    var producteurNomPrenoms = ""
    var typeProduit = ""
    var producteurId = ""
    var delegueID = ""
    var delegueNom = ""
    var dateLivraison = ""

    var campagneNom = ""
    var campagneId = ""

    var magasinNom = ""
    var magasinId = ""

    var parcelleNom = ""
    var parcelleSuperficie = ""
    var parcelleId = ""

    var datePickerDialog: DatePickerDialog? = null
    var draftedDataLivraison: DataDraftedModel? = null


    fun setupMagasinSelection(delegue: Int) {
        magasinDao = AnouanzeRoomDatabase.getDatabase(applicationContext)?.magasinSectionDao()
        magasinsList = magasinDao?.getDelegueMagasins(delegue)

        val magasinAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, magasinsList!!)
        selectMagasinSectionLivraison!!.adapter = magasinAdapter

        selectMagasinSectionLivraison.setTitle("Choisir le magasin")
        selectMagasinSectionLivraison.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                val magasin = magasinsList!![position]
                magasinNom = magasin.nomMagasinsections!!
                magasinId = magasin.id.toString()

                LogUtils.e(Commons.TAG, "ID -> $magasinId")
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }
    }


    fun setupLocaliteSelection() {
        localiteDao = AnouanzeRoomDatabase.getDatabase(applicationContext)?.localiteDoa()
        localitesList = localiteDao?.getAll(agentID = SPUtils.getInstance().getInt(Constants.AGENT_ID, 0).toString()) ?: mutableListOf()

        if (localitesList?.size == 0) {
            showMessage(
                "La liste des localités est vide ! Refaite une mise à jour.",
                this,
                finished = false,
                callback = {},
                "OKAY",
                false,
                showNo = false,
            )

            localiteIdSelected = ""
            localiteSelected = ""
            selectLocaliteFormation?.adapter = null
            return
        }

        val localiteAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, localitesList!!)
        selectLocaliteLivraison!!.adapter = localiteAdapter

        selectLocaliteLivraison.setTitle("Choisir la localite")

        selectLocaliteLivraison.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                val locality = localitesList!![position]
                localiteSelected = locality.nom!!

                localiteIdSelected = if (locality.isSynced) {
                    locality.id!!.toString()
                } else {
                    locality.uid.toString()
                }

                setupProducteurSelection(localiteIdSelected)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }
    }


    fun setupCampagneSelection() {
        campagneDao = AnouanzeRoomDatabase.getDatabase(applicationContext)?.campagneDao()
        campagnesList = campagneDao?.getAll()

        val campagneAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, campagnesList!!)
        selectCampagneLivraison!!.adapter = campagneAdapter

        selectCampagneLivraison.setTitle("Choisir la campagne")

        selectCampagneLivraison.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                val campagne = campagnesList!![position]
                campagneNom = campagne.campagnesNom!!
                campagneId = campagne.id.toString()
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }
    }


    fun setupParcellesProducteurSelection(producteurId: String?) {
        parcelleDao = AnouanzeRoomDatabase.getDatabase(applicationContext)?.parcelleDao()
        parcellesList = parcelleDao?.getParcellesProducteur(producteurId = producteurId, agentID = SPUtils.getInstance().getInt(Constants.AGENT_ID, 0).toString())

        val parcellesAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, parcellesList!!)
        selectParcelleLivraison!!.adapter = parcellesAdapter

        selectParcelleLivraison.setTitle("Choisir la parcelle")
        selectParcelleLivraison.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                val parcelle = parcellesList!![position]

                parcelleNom = "${parcelle.culture?:Constants.VIDE} (${parcelle.anneeCreation?:Constants.VIDE})"
                parcelleSuperficie = parcelle.superficie!!

                if (parcelle.isSynced) {
                    parcelleId = parcelle.id.toString()
                } else {
                    parcelleId = parcelle.id.toString()
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }

    }


    fun setupDelegueSelection() {
        delegueDao = AnouanzeRoomDatabase.getDatabase(applicationContext)?.delegueDao()
        deleguesList = delegueDao?.getAll(agentID = SPUtils.getInstance().getInt(Constants.AGENT_ID, 0).toString())

        val delegueAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, deleguesList!!)
        selectDelegueLivraison!!.adapter = delegueAdapter

        selectDelegueLivraison.setTitle("Choisir le délégué")

        selectDelegueLivraison.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                val delegue = deleguesList!![position]
                delegueID = delegue.id.toString()
                delegueNom = delegue.nom!!

                setupMagasinSelection(delegueID.toInt())
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }
    }


    fun setupTypeProduitSelection() {
        //typeProduitDao = AnouanzeRoomDatabase.getDatabase(applicationContext)?.typeProduitDao()
        val typeProduitssList = AssetFileHelper.getListDataFromAsset(12, this@LivraisonActivity) as MutableList<TypeProduitModel>?
            //typeProduitDao?.getAll(agentID = SPUtils.getInstance().getInt(Constants.AGENT_ID, 0).toString())

        val tyeProduitAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, typeProduitssList!!)
        selectTypeProduitLivraison!!.adapter = tyeProduitAdapter

        selectTypeProduitLivraison.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                val typeProduitData = typeProduitssList[position]
                typeProduit = typeProduitData.nom ?: ""
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }
    }


    fun setupProducteurSelection(pLocaliteId: String) {
        producteursList = AnouanzeRoomDatabase.getDatabase(applicationContext)?.producteurDoa()?.getProducteursByLocalite(localite = pLocaliteId)
        val producteursDatas: MutableList<CommonData> = mutableListOf()
        producteursList?.map {
            CommonData(id = it.id, nom = "${it.nom} ${it.prenoms}")
        }?.let {
            producteursDatas.addAll(it)
        }

        val livraisonDrafted = ApiClient.gson.fromJson(draftedDataLivraison?.datas, LivraisonModel::class.java)
        selectProducteurLivraison.adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, producteursDatas)

        if (livraisonDrafted != null) {
            provideDatasSpinnerSelection(
                selectProducteurLivraison,
                livraisonDrafted.producteurNom,
                producteursDatas
            )
        }

        selectProducteurLivraison.setTitle("Choisir le producteur")
        selectProducteurLivraison.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                val producteur = producteursList!![position]
                producteurNomPrenoms = "${producteur.nom} ${producteur.prenoms}"

                producteurId = if (producteur.isSynced) {
                    producteur.id!!.toString()
                } else {
                    producteur.uid.toString()
                }

                setupParcellesProducteurSelection(producteurId)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }
    }


    fun setAll() {
        setupLocaliteSelection()
        setupDelegueSelection()
        setupCampagneSelection()
        setupTypeProduitSelection()
    }


    fun collectDatas() {
        if (parcelleId.isBlank()) {
            showMessage(
                "Aucune parcelle selectionnée !",
                this,
                finished = false,
                callback = {},
                deconnec = false,
                positive = "OKAY",
                showNo = false
            )
            return
        }

        if (campagneId.isBlank()) {
            showMessage(
                "Aucune campagne selectionnée !",
                this,
                finished = false,
                callback = {},
                deconnec = false,
                positive = "OKAY",
                showNo = false
            )
            return
        }

        if (delegueID.isBlank()) {
            showMessage(
                "Aucun délégué selectionné !",
                this,
                finished = false,
                callback = {},
                deconnec = false,
                positive = "OKAY",
                showNo = false
            )
            return
        }

        if (magasinId.isBlank()) {
            showMessage(
                "Aucun magasin selectionné !",
                this,
                finished = false,
                callback = {},
                deconnec = false,
                positive = "OKAY",
                showNo = false
            )
            return
        }

        if (editVolumeLivraison.text?.trim().toString().isBlank()) {
            showMessage(
                "Aucun vlume renseigné !",
                this,
                finished = false,
                callback = {},
                deconnec = false,
                positive = "OKAY",
                showNo = false
            )
            return
        }

        val livraisonModel = LivraisonModel(
            uid = 0,
            id = 0,
            dateLivre = dateLivraison,
            delegueId = delegueID,
            nombreSacs = editNombreSacsLivraison.text?.trim().toString().ifBlank { "0" },
            producteursId = producteurId,
            quantiteLivre = editVolumeLivraison.text?.trim().toString(),
            isSynced = false,
            agentId = SPUtils.getInstance().getInt(Constants.AGENT_ID, 0).toString(),
            origin = "local",
            campagneId = campagneId,
            typeProduit = typeProduit,
            campagneNom = campagneNom,
            producteurNom = producteurNomPrenoms,
            localiteNom = localiteSelected,
            parcelleId = parcelleId,
            parcelleNom = parcelleNom,
            delegueNom = delegueNom,
            magasinSectionId = magasinId,
            magasinSectionLabel = magasinNom
        )

       try {
           val intentLivraisonPreview = Intent(this, LivraisonPreviewActivity::class.java)
           intentLivraisonPreview.putExtra("preview", livraisonModel)
           intentLivraisonPreview.putExtra("draft_id", draftedDataLivraison?.uid)
           startActivity(intentLivraisonPreview)
       } catch (ex: Exception) {
           ex.toString()
       }
    }


    fun clearFields() {
        setAll()

        editDateLivraison.text = null
        editVolumeLivraison.text = null
        editNombreSacsLivraison.text = null

        delegueID = ""
        producteurId = ""

        selectDelegueLivraison.setSelection(0)
        selectLocaliteLivraison.setSelection(0)
    }


    fun draftLivraison(draftModel: DataDraftedModel?) {
        val livraisonModelDraft = LivraisonModel(
            uid = 0,
            id = 0,
            dateLivre = dateLivraison,
            delegueId = delegueID,
            nombreSacs = editNombreSacsLivraison.text?.trim().toString(),
            producteursId = producteurId,
            quantiteLivre = editVolumeLivraison.text?.trim().toString(),
            isSynced = false,
            agentId = SPUtils.getInstance().getInt(Constants.AGENT_ID, 0).toString(),
            origin = "local",
            typeProduit = typeProduit,
            campagneId = campagneId,
            campagneNom = campagneNom,
            producteurNom = producteurNomPrenoms,
            localiteNom = localiteSelected,
            parcelleId = parcelleId,
            parcelleNom = parcelleNom,
            delegueNom = delegueNom,
            magasinSectionId = magasinId,
            magasinSectionLabel = magasinNom
        )

        Commons.showMessage(
            message = "Voulez-vous vraiment mettre ce contenu au brouillon afin de reprendre ulterieurement ?",
            context = this,
            finished = false,
            callback = {
                AnouanzeRoomDatabase.getDatabase(this)?.draftedDatasDao()?.insert(
                    DataDraftedModel(
                        uid = draftModel?.uid ?: 0,
                        datas = ApiClient.gson.toJson(livraisonModelDraft),
                        typeDraft = "livraison",
                        agentId = SPUtils.getInstance().getInt(Constants.AGENT_ID).toString()
                    )
                )

                Commons.showMessage(
                    message = "Contenu ajouté aux brouillons !",
                    context = this,
                    finished = true,
                    callback = {
                        Commons.playDraftSound(this)
                        imageDraftLivraison.startAnimation(Commons.loadShakeAnimation(this))
                    },
                    positive = "OK",
                    deconnec = false,
                    false
                )
            },
            positive = "OUI",
            deconnec = false,
            showNo = true
        )
    }


    fun undraftedDatas(draftedData: DataDraftedModel) {
        val livraisonDrafted = ApiClient.gson.fromJson(draftedData.datas, LivraisonModel::class.java)

        // Localite
        val localitesLists = AnouanzeRoomDatabase.getDatabase(this)?.localiteDoa()?.getAll(SPUtils.getInstance().getInt(Constants.AGENT_ID, 0).toString())
        val localitesDatas: MutableList<CommonData> = mutableListOf()
        localitesLists?.map {
            CommonData(id = it.id, nom = it.nom)
        }?.let {
            localitesDatas.addAll(it)
        }
        selectLocaliteLivraison.adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, localitesDatas)
        provideDatasSpinnerSelection(
            selectLocaliteLivraison,
            livraisonDrafted.localiteNom,
            localitesDatas
        )

        // Campagne
        val campagnesLists = AnouanzeRoomDatabase.getDatabase(this)?.campagneDao()?.getAll()
        val campagnesDatas: MutableList<CommonData> = mutableListOf()
        campagnesLists?.map {
            CommonData(id = it.id, nom = it.campagnesNom)
        }?.let {
            campagnesDatas.addAll(it)
        }
        selectCampagneLivraison.adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, campagnesDatas)
        provideDatasSpinnerSelection(
            selectCampagneLivraison,
            livraisonDrafted.campagneNom,
            campagnesDatas
        )

        // Delegue
        val deleguesLists = AnouanzeRoomDatabase.getDatabase(this)?.delegueDao()?.getAll(SPUtils.getInstance().getInt(Constants.AGENT_ID, 0).toString())
        val deleguesDatas: MutableList<CommonData> = mutableListOf()
        deleguesLists?.map {
            CommonData(id = it.id, nom = it.nom)
        }?.let {
            deleguesDatas.addAll(it)
        }
        selectDelegueLivraison.adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, deleguesDatas)
        provideDatasSpinnerSelection(
            selectDelegueLivraison,
            livraisonDrafted.delegueNom,
            deleguesDatas
        )

        // Type Produit
        provideStringSpinnerSelection(
            spinner = selectTypeProduitLivraison,
            value = livraisonDrafted.typeProduit,
            list = resources.getStringArray(R.array.type_produit)
        )

        editVolumeLivraison.setText(livraisonDrafted.quantiteLivre)
        editNombreSacsLivraison.setText(livraisonDrafted.nombreSacs)

        editDateLivraison.setText(livraisonDrafted.dateLivre)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_livraison)
        livraisonDao = AnouanzeRoomDatabase.getDatabase(this)?.livraisonDao()



        editDateLivraison.setOnClickListener {
            datePickerDialog = null
            val calendar: Calendar = Calendar.getInstance()
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
            datePickerDialog = DatePickerDialog(this, { p0, year, month, day ->

                //LogUtils.e(TAG, Commons.convertDate("${day}-${(month + 1)}-$year", false))
                editDateLivraison.setText(Commons.convertDate("${day}-${(month + 1)}-$year", false))
                dateLivraison = editDateLivraison.text?.toString()!!
            }, year, month, dayOfMonth)

            datePickerDialog!!.datePicker.maxDate = Date().time
            datePickerDialog?.show()
        }

        clickCancelLivraison.setOnClickListener {
            clearFields()
        }

        clickSaveLivraison.setOnClickListener {
            collectDatas()
        }

        clickCloseLivraison.setOnClickListener {
            finish()
        }

        imageDraftLivraison.setOnClickListener {
            draftLivraison(draftedDataLivraison ?: DataDraftedModel(uid = 0))
        }

        setAll()

        editVolumeLivraison.doAfterTextChanged {
            try {

            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        editNombreSacsLivraison.doAfterTextChanged {
            try {

            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        applyFilters(editNombreSacsLivraison, withZero = true)
        applyFilters(editVolumeLivraison, withZero = true)

        if (intent.getStringExtra("from") != null) {
            draftedDataLivraison = AnouanzeRoomDatabase.getDatabase(this)?.draftedDatasDao()?.getDraftedDataByID(intent.getIntExtra("drafted_uid", 0)) ?: DataDraftedModel(uid = 0)
            undraftedDatas(draftedDataLivraison!!)
        }
    }
}
