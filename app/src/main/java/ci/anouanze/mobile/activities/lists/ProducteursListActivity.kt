package ci.anouanze.mobile.activities.lists

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import ci.anouanze.mobile.R
import ci.anouanze.mobile.activities.forms.ProducteurActivity
import ci.anouanze.mobile.adapters.ProducteurAdapter
import ci.anouanze.mobile.models.ProducteurModel
import ci.anouanze.mobile.repositories.databases.AnouanzeRoomDatabase
import ci.anouanze.mobile.repositories.databases.daos.ProducteurDao
import ci.anouanze.mobile.tools.Constants
import com.blankj.utilcode.util.ActivityUtils
import com.blankj.utilcode.util.SPUtils
import kotlinx.android.synthetic.main.activity_localites_list.*
import kotlinx.android.synthetic.main.activity_producteur_menage.*
import kotlinx.android.synthetic.main.activity_producteurs_list.*
import org.joda.time.DateTime

class ProducteursListActivity : AppCompatActivity() {


    var producteurDao: ProducteurDao? = null
    var producteursList: MutableList<ProducteurModel>? = null
    var producteurAdapter: ProducteurAdapter? = null


    fun retrieveDatas() {
        producteursList = mutableListOf()
        producteurDao = AnouanzeRoomDatabase.getDatabase(this)?.producteurDoa()

        producteursList = producteurDao?.getUnSyncedAll(agentID = SPUtils.getInstance().getInt(
            Constants.AGENT_ID, 0).toString())

        producteurAdapter = ProducteurAdapter(producteursList)

        recyclerProducteurs.adapter = producteurAdapter
        recyclerProducteurs.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        labelLastSynchronisationProducteur.text = resources.getString(R.string.last_synchronisation_date, DateTime.now().toString("HH:mm:ss"))

        producteursList?.let {
            if (it.isEmpty()) {
                recyclerProducteurs.visibility = View.GONE
                linearEmptyContainerProducteursList.visibility = View.VISIBLE
            } else {
                recyclerProducteurs.visibility = View.VISIBLE
                linearEmptyContainerProducteursList.visibility = View.GONE
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_producteurs_list)

        imgAddProducteur.setOnClickListener {
            ActivityUtils.startActivity(ProducteurActivity::class.java)
        }

        clickCloseProducteurs.setOnClickListener {
            finish()
        }
    }


    override fun onResume() {
        super.onResume()

        retrieveDatas()
    }


}
