package ci.anouanze.mobile.tests

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ci.anouanze.mobile.R
import ci.anouanze.mobile.tools.Commons
import com.blankj.utilcode.util.ToastUtils

class TestLabActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_lab)


        Commons.showMessage(
            "Clic sur mon",
            this,
            finished = false,
            callback = { ToastUtils.showShort("merci") },
            deconnec = false
        )
    }
}
