package ci.anouanze.mobile.tests

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ci.anouanze.mobile.R

class TestExportKmlActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_export_kml)
    }
}
