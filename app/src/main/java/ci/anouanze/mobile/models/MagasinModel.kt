package ci.anouanze.mobile.models


import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ci.anouanze.mobile.tools.Constants
import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose


@Entity(tableName = Constants.TABLE_MAGASIN, indices = [Index(value = ["id"], unique = true)])
data class MagasinModel(
    @Expose @PrimaryKey(autoGenerate = true) val uid: Int,
    @SerializedName("codeMagasinsections") @Expose var codeMagasinsections: String? = "",
    @SerializedName("created_at") @Expose var createdAt: String? = "",
    @SerializedName("delegues_id") @Expose var deleguesId: Int? = 0,
    @SerializedName("deleted_at") @Expose var deletedAt: String? = "",
    @SerializedName("id") @Expose var id: Int? = 0,
    @SerializedName("nomMagasinsections") @Expose var nomMagasinsections: String? = "",
    @SerializedName("updated_at") @Expose var updatedAt: String? = "",
) {
    override fun toString(): String {
        return nomMagasinsections.toString()
    }
}
