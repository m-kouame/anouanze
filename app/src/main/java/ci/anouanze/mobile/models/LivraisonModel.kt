package ci.anouanze.mobile.models


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ci.anouanze.mobile.tools.Constants
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = Constants.TABLE_LIVRAISONS, indices = [Index(value = ["uid"], unique = true)])
@Parcelize
data class LivraisonModel(
    @Expose @PrimaryKey(autoGenerate = true) var uid: Int,
    @Expose val id: Int? = 0,
    @SerializedName("dateLivre")
    @Expose var dateLivre: String? = "",
    @SerializedName("nomDelegue") @Expose var delegueId: String? = "",
    @Expose var delegueNom: String? = "",
    @Expose var typeProduit: String? = "",
    @SerializedName("nombreSacs")
    @Expose var nombreSacs: String? = "",
    @SerializedName("producteurs_id") @Expose var producteursId: String? = "",
    @Expose var producteurNom: String? = "",
    @SerializedName("campagnes_id") @Expose var campagneId: String? = "",
    @Expose var campagneNom: String? = "",
    @SerializedName("parcelles_id") @Expose var parcelleId: String? = "",
    @Expose var parcelleNom: String? = "",
    @SerializedName("quantiteLivre")
    @Expose var quantiteLivre: String? = "",
    @Expose var localiteNom: String? = "",
    var isSynced: Boolean = false,
    @Expose @SerializedName("userid") var agentId: String? = "",
    @Expose @SerializedName("magasinsections_id") var magasinSectionId: String? = "",
    @Expose var magasinSectionLabel: String? = "",
    var origin: String? = "local"
) : Parcelable
