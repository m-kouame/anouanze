package ci.anouanze.mobile.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ci.anouanze.mobile.tools.Constants
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Entity(tableName = Constants.TABLE_MENAGES, indices = [Index(value = ["id"], unique = true)])
@Parcelize
data class ProducteurMenageModel(
    @Expose @PrimaryKey(autoGenerate = true) var uid: Long = 0,
    @Expose var id: String? = "",
    @Expose var activiteFemme: String? = "",
    @Expose var boisChauffe: String? = "",
    @Expose var champFemme: String? = "",
    @Expose var eauxToillette: String? = "",
    @Expose var eauxVaisselle: String? = "",
    @Expose var empruntMachine: String? = "",
    @Expose var equipements: String? = "",
    @Expose var gardeEmpruntMachine: String? = "",
    @Expose var garde_machines_id: String? = "",
    @Expose var machine: String? = "",
    @Expose var nomActiviteFemme: String? = "",
    @Expose var nomPersonneTraitant: String? = "",
    @Expose var nombreHectareFemme: String? = "",
    @Expose var ordures_menageres_id: String? = "",
    @Expose var producteurs_id: String? = "",
    @Expose var producteurNomPrenoms: String? = "",
    @Expose var quartier: String? = "",
    @Expose var separationMenage: String? = "",
    @Expose var sources_eaux_id: String? = "",
    @Expose var sources_energies_id: String? = "",
    @Expose var superficieCacaoFemme: String? = "",
    @Expose var traitementChamps: String? = "",
    @Expose var type_machines_id: String? = "",
    @Expose var numeroPersonneTraitant: String? = "",
    var codeProducteur: String? = "",
    @Expose var wc: String? = "",
    @Expose var localiteNom: String? = "",
    var isSynced: Boolean = false,
    @Expose @SerializedName("userid") val agentId: String? = "",
    var origin: String? = "local"
) : Parcelable
