package ci.anouanze.mobile.models


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import ci.anouanze.mobile.tools.Constants
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Entity(tableName = Constants.TABLE_SUIVI_APPLICATION,
    indices = [
        Index(
            value = ["uid"],
            unique = true
        )
    ]
)
@Parcelize
data class SuiviApplicationModel(
    @SerializedName("applicateurs_id") @Expose var applicateursId: String? = "",
    @SerializedName("campagnes_id") @Expose var campagnesId: Int? = 0,
    @Expose var cultureNom: String? = "",
    @Expose var producteurNom: String? = "",
    @Expose var applicateurNom: String? = "",
    @Expose var localiteNom: String? = "",
    @Expose var campagneNom: String? = "",
    @SerializedName("dateApplication") @Expose var dateApplication: String? = "",
    @SerializedName("degreDangerosite") @Expose var degreDangerosite: String? = "",
    @SerializedName("delaisReentree") @Expose var delaisReentree: String? = "",
    @SerializedName("heureApplication") @Expose var heureApplication: String? = "",
    @SerializedName("marqueProduitPulverise") @Expose var marqueProduitPulverise: String? = "",
    @SerializedName("matieresActivesStringify") @Expose var matieresActivesStringify: String? = "",
    @SerializedName("nomInsectesCiblesStringify") @Expose var nomInsectesCiblesStringify: String?  = "",
    @SerializedName("parcelles_id") @Expose var parcellesId: String? = "",
    @SerializedName("photoDouche") @Expose var photoDouche: String? = "",
    @SerializedName("photoZoneTampons") @Expose var photoZoneTampons: String? = "",
    @SerializedName("presenceDouche") @Expose var presenceDouche: String? = "",
    @SerializedName("raisonApplication") @Expose var raisonApplication: String? = "",
    @SerializedName("superficiePulverisee") @Expose var superficiePulverisee: String? = "",
    @SerializedName("uid") @Expose @PrimaryKey(autoGenerate = true) var uid: Int,
    @SerializedName("userid") @Expose var userid: Int? = 0,
    @SerializedName("zoneTampons") @Expose var zoneTampons: String? = "",
    var photoTamponPath: String? = "",
    var photoDouchePath: String? = "",
    val isSynced: Boolean = false,
    @Expose var id: Int? = 0,
    var origin: String? = "local"
) : Parcelable {
    @Ignore @SerializedName("matieresActives") @Expose(serialize = true, deserialize = false) var matieresActives: MutableList<String>? = mutableListOf()
    @Ignore @SerializedName("nomInsectesCibles") @Expose(serialize = true, deserialize = false) var nomInsectesCibles: MutableList<String>? = mutableListOf()
}
