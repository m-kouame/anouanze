package ci.anouanze.mobile.repositories.databases.daos

import androidx.room.*
import ci.anouanze.mobile.models.AgentModel
import ci.anouanze.mobile.models.CourEauModel
import ci.anouanze.mobile.models.IntrantModel
import ci.anouanze.mobile.models.TypeFormationModel
import ci.anouanze.mobile.tools.Constants

/**
 *  Created by didierboka.developer on 18/12/2021
 *  mail for work:   (didierboka.developer@gmail.com)
 */

@Dao
interface TypeFormationDao {

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(typeFormationModel: TypeFormationModel)

    @Transaction
    @Query("SELECT * FROM type_formation WHERE agentId = :agentID")
    fun getAll(agentID: String?): MutableList<TypeFormationModel>

    @Transaction
    @Query("DELETE FROM type_formation")
    fun deleteAll()
}
