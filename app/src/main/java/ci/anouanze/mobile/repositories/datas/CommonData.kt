package ci.anouanze.mobile.repositories.datas

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CommonData (
    @Expose val id: Int? = null,
    @Expose val nom: String? = null,
    @Expose val table: String? = null,
    @Expose val codeapp: String? = null,
    @Expose val userid: Int? = null,
    @Expose val typeFormationId: Int? = null,
    @Expose @SerializedName("cooperativesid", alternate = ["cooperative_id"]) val cooperativeId: Int? = null
) {
    override fun toString(): String {
        return nom!!
    }
}
