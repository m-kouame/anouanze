package ci.anouanze.mobile.repositories.databases.daos

import androidx.room.*
import ci.anouanze.mobile.models.AgentModel
import ci.anouanze.mobile.models.CampagneModel
import ci.anouanze.mobile.models.CourEauModel
import ci.anouanze.mobile.tools.Constants

/**
 *  Created by didierboka.developer on 18/12/2021
 *  mail for work:   (didierboka.developer@gmail.com)
 */

@Dao
interface CampagneDao {

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(campagneModel: CampagneModel)

    @Transaction
    @Query("SELECT * FROM campagne")
    fun getAll(): MutableList<CampagneModel>

    @Transaction
    @Query("DELETE FROM campagne")
    fun deleteAll()
}
