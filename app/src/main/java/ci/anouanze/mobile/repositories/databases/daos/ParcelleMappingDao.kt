package ci.anouanze.mobile.repositories.databases.daos

import androidx.room.*
import ci.anouanze.mobile.models.AgentModel
import ci.anouanze.mobile.models.CourEauModel
import ci.anouanze.mobile.models.ParcelleMappingModel
import ci.anouanze.mobile.tools.Constants

/**
 *  Created by didierboka.developer on 18/12/2021
 *  mail for work:   (didierboka.developer@gmail.com)
 */

@Dao
interface ParcelleMappingDao {

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(parcelleMappingModel: ParcelleMappingModel)

    @Transaction
    @Query("SELECT * FROM parcelle_mapping WHERE producteurId = :producteurID")
    fun getProducteurParcellesList(producteurID: String?): MutableList<ParcelleMappingModel>

    @Transaction
    @Query("DELETE FROM parcelle_mapping")
    fun deleteAll()
}
