package ci.anouanze.mobile.repositories.databases.daos

import androidx.room.*
import ci.anouanze.mobile.models.AgentModel
import ci.anouanze.mobile.models.CourEauModel
import ci.anouanze.mobile.models.IntrantModel
import ci.anouanze.mobile.tools.Constants

/**
 *  Created by didierboka.developer on 18/12/2021
 *  mail for work:   (didierboka.developer@gmail.com)
 */

@Dao
interface IntrantDao {

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(intrant: IntrantModel)

    @Transaction
    @Query("SELECT * FROM intrant WHERE agentId = :agentID")
    fun getAll(agentID: String?): MutableList<IntrantModel>

    @Transaction
    @Query("DELETE FROM intrant")
    fun deleteAll()
}